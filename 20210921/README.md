# Appunti della lezione di martedì 21 settembre 2021

## Nelle scorse lezioni
Abbiamo capito fino ad ora come fare ad estrarre dei descrittori sia nel dominio del tempo che nel dominio della frequenza e come fare a visualizzarli.

Oggi parleremo e vedremo altri descrittori che realizzano un tipo di analisi molto importante nel campo della MIR, ovvero:
- descrittori del pitch, dell'intonazione di un segnale
- MFCC: analisi importante ed usata nei progetti che riguardano l'analisi del segnale audio in generale per MFCC intendiamo la Mel Frequency Cepstrum Coefficents, ovvero Cofficienti Cepstrum su scala Mel.

## Pitch tracking

Descrittori che si occupano di fare una analisi del pitch del segnale.

Non tutti i segnali audio sono dotati di pitch.

Il quinto script in sostanza era una rappresentazione grafica di descrittori precedenti.

### Script sul pitch tracking

Osserviamo lo script `computePitch` di Alexander Lerch per vedere quali siano i tracker supportati:
```
# -*- coding: utf-8 -*-
"""
computePitch

computes the fundamental frequency of the (monophonic) audio
supported pitch trackers are:
    'SpectralAcf',
    'SpectralHps',
    'TimeAcf',
    'TimeAmdf',
    'TimeAuditory',
    'TimeZeroCrossings',
  Args:
      cPitchTrackName: feature to compute, e.g. 'SpectralHps'
      afAudioData: array with floating point audio data.
      f_s: sample rate
      afWindow: FFT window of length iBlockLength (default: hann)
      iBlockLength: internal block length (default: 4096 samples)
      iHopLength: internal hop length (default: 2048 samples)

  Returns:
      f frequency
      t time stamp for the frequency value
"""

import numpy as np
from scipy.signal import spectrogram
import matplotlib.pyplot as plt

import pyACA
from pyACA.ToolPreprocAudio import ToolPreprocAudio
from pyACA.ToolComputeHann import ToolComputeHann
from pyACA.ToolReadAudio import ToolReadAudio


def computePitch(cPitchTrackName, afAudioData, f_s, afWindow=None, iBlockLength=4096, iHopLength=2048):

    #mypackage = __import__(".Pitch" + cPitchTrackName, package="pyACA")
    hPitchFunc = getattr(pyACA, "Pitch" + cPitchTrackName)

    # pre-processing
    afAudioData = ToolPreprocAudio(afAudioData, iBlockLength)

    if isSpectral(cPitchTrackName):
        # compute window function for FFT
        if afWindow is None:
            afWindow = ToolComputeHann(iBlockLength)

        assert(afWindow.shape[0] == iBlockLength), "parameter error: invalid window dimension"

        # in the real world, we would do this block by block...
        [f_k, t, X] = spectrogram(afAudioData,
                                  f_s,
                                  afWindow,
                                  iBlockLength,
                                  iBlockLength - iHopLength,
                                  iBlockLength,
                                  False,
                                  True,
                                  'spectrum')

        # we just want the magnitude spectrum...
        X = np.sqrt(X / 2)

        # compute instantaneous pitch chroma
        f = hPitchFunc(X, f_s)

    if isTemporal(cPitchTrackName):
        [f, t] = hPitchFunc(afAudioData, iBlockLength, iHopLength, f_s)

    return (f, t)


#######################################################
# helper functions
def isSpectral(cName):
    bResult = False
    if "Spectral" in cName:
        bResult = True

    return (bResult)


def isTemporal(cName):
    bResult = False
    if "Time" in cName:
        bResult = True

    return (bResult)


def computePitchCl(cPath, cPitchTrackName, bPlotOutput=False):

    # read audio file
    [f_s, afAudioData] = ToolReadAudio(cPath)
    # afAudioData = np.sin(2*np.pi * np.arange(f_s*1)*440./f_s)

    # compute feature
    [v, t] = computePitch(cPitchTrackName, afAudioData, f_s)

    # plot feature output
    if bPlotOutput:
        plt.plot(t, v)

    return (v, t)


if __name__ == "__main__":
    import argparse

    # add command line args and parse them
    parser = argparse.ArgumentParser(description='Compute key of wav file')
    parser.add_argument('--infile', metavar='path', required=False,
                        help='path to input audio file')
    parser.add_argument('--featurename', metavar='string', required=False,
                        help='feature name in the format SpectralPitchChroma')
    parser.add_argument('--plotoutput', metavar='bool', required=False,
                        help='option to plot the output')

    # retrieve command line args
    args = parser.parse_args()
    cPath = args.infile
    cPitchTrackName = args.featurename
    bPlotOutput = args.plotoutput

    # only for debugging
    if __debug__:
        if not cPath:
            cPath = "c:/temp/test.wav"
        if not cPitchTrackName:
            cPitchTrackName = "TimeAmdf"
        if not bPlotOutput:
            bPlotOutput = True

    # call the function
    computePitchCl(cPath, cPitchTrackName, bPlotOutput)
```

### `TimeAcf`

>I descrittori possono lavorare nel dominio del tempo e nel dominio della frequenza, mentre i tracker lavorano sempre nel dominio della frequenza e del tempo, ma essi sono descrittori di medio livello che seguono la linea dell'intonazione del materiale audio.

Questo tipo di analisi lavora nel dominio del tempo.

ACF sta per Auto-Correlation Function, in generale la correlazione tra due segnali è uno strumento matematico che consente di far vedere se due segnali condividono delle peridicità, come si calcola l'autocorrelazione?

Essa è in realtà un'operazione semplice...

##### Funzione di correlazione e autocorrelazione
La funzione di correlazione si fa tra due segnali fra loro, cercando di capire cosa c'è in comune fra i due segnali dal punto di vista della periodicità.

Quando il segnale è lo stesso(e non abbiamo due segnali da analizzare) essa si chiama autocorrelazione.

Permette quindi di osservare nel tempo la periodicità del segnale.

>La correlazione è una versione piú semplice della convoluzione.

L'autocorrelazione prende una copia del segnale cosí com'è, ne fa un clone, lo trasla di un delta_t:
![/img/auto](/img/auto.png)

Facciamo nel tempo il prodotto dei campioni dei due segnali, ne facciamo il prodotto e sommiamo tutto ciò che viene fuori.

Prodotto dei campioni dei due segnali per ognuno dei due campioni e poi li sommo.

In questo caso il segnale clonato viene solo spostato e non girato sull'asse delle x.

Il segnale viene shiftato anche sulla _correlazione_.

Parlando di finestre, ovvero di segnali limitati nel tempo, avremo dei prodotti fra i segnali e quindi le regioni che non si sovrappongono varranno pari a zero.

La correlazione rappresentabile con il simbolo matematico:
![/img/A_xx_k_=_sum_n=1.png](/img/A_xx_k_=_sum_n=1.png)


Si prende il segnale originale, il segnale traslato, si fa il prodotto tra i risultati dei campioni che provengono da questa moltiplicazioni.

_Quale valore ci si aspetta del valore di Axx[0]?_

Ovvero l'energia del segnale, ed e1 sicuramente il valore massimo.

Senza traslare il segnale abbiamo il valore dell'energia massimo.

Pensiamo ad un segnale particolare ovvero un segnale sinusoidale, quando la seconda sinusoide è traslata perfettamente di un periodo, lo shift è 0.

![/img/si](/img/si.png)

>Ricavare una mappa delle periodicità contenute nel segnale.

Se il segnale ha un periodo, vuol dire che se faccio questo gioco matematico mi farà ottenere dei picchi.

Il valore iniziale di Axx[0] viene scartato perchè sappiamo che esso mi da il valore dell'energia del segnale e non mi da informazioni sulla periodicità del segnale.
![/img/A_xxt](/img/A_xxt.png)

Nel caso della sinusoide matematica è anche lei una sinusoide con i picchi tutti uguali, ciò e1 vero perchè esso e1 un segnale ideale infinito.

Avendo invece un sengale reale, ovvero un trancio di sinusoide, avremo a quel punto un segnale che si spenge.

![/img/au](/img/au.png)

Avremo quindi il periodo della sinusoide che stiamo analizzando.

>I radar funzionano esattamente allo stesso modo, sparando un segnale e calcolando il tempo di ritardo del segnale che ritorna.

#### Pitch tracking su Audacity

Vediamo su Audacity che la forma d'onda è periodica.

Analizzando un intervallo di una forma d'onda con l'analisi della frequenza:

![/img/auda](/img/auda.png)

Vediamo che quest'algoritmo si calcola l'autocorrelazione e per ognuna delle frame si va a cercare il massimo più vicino a quello dello zero.

L'autocorrelazione è un segnale pari e a noi interesserà la sola parte positiva.

>L'autocorrelazione di un rumore bianco ideale è un rumore bianco.

Per le applicazioni di crittografia voglio delle frequenze veramente random, perchè nel caso si verifichi una periodicità posso coglierla e fare delle autocorrelazioni e creare similitudini tra un segnale e l'altro.

#### Visualizzazione del `TimeAcf`

![/img/trombone](/img/trombone.png)

Abbiamo un'instabilità sulla parte inziale e finale del grafico.

>Nel trombone il segnale che do, è un impulso di aria che da energia a tante componenti del segnale.

Vi è un descrittore anche per il tempo di attacco che descrive e individua cosa sia l'attacco.

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "roar_tiger_mono.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

time = np.arange(0,np.size(x))/fs

#calcolo dei descrittori
N = 1024
hopSize = 512
#RMS
[dsrms, t] = pyACA.computeFeature("TimeRms",x,fs,iBlockLength=N, iHopLength=hopSize)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=N, iHopLength=hopSize)
#pitch tracker
[vP, tP] = pyACA.computePitch("TimeAcf",x,fs,iBlockLength=N, iHopLength=hopSize)

#plottiamo

#mi ricavo gli handler della chiamata a subplots(2 righe, una colonna)
fig, axs = plt.subplots(3,1)

#configuro il primo grafico attraverso il primo oggetto della lista di oggetti che fanno riferimento agli assi
color = 'silver'
axs[0].grid()
axs[0].set_xlabel("Tempo", color=color)
axs[0].set_ylabel("Segnale", color=color)
axs[0].plot(time, x, color=color)

#clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
ax2 = axs[0].twinx()

color = 'tab:red'
ax2.grid()
ax2.plot(t,dsrms)
ax2.set_xlabel("Tempo", color=color)
ax2.set_ylabel("RMS", color=color)

#configuro il grafico della seconda finestra
#plot del flusso spettrale
color = 'tab:blue'
axs[1].grid()
axs[1].plot(t,dsx)
#axs[1].set_xlabel("Tempo", color=color)
axs[1].set_ylabel("Spectral Flux", color=color)

#configuro il grafico della terza finestra
#plot del pitch tracker
color = 'tab:green'
axs[2].grid()
axs[2].plot(tP,vP)
#axs[2].set_xlabel("Tempo", color=color)
axs[2].set_ylabel("Pitch", color=color)


plt.show()
```
Abbiamo visualizzato nel tempo la funzione di autocorrelazione.

### `SpectralAcf`

Essa fa la stessa cosa della `TimeAcf`, ma nel dominio della frequenza e vi sono vantaggi matematici.

Pensando allo spettro del segnale, anche esso è un segnale periodico, e posso calcolarmi la periodicità del segnale.

L'autocorrelazione spettrale si fa in genere per evitare l'autocorrelazione nel tempo.

L'autocorrelazione temporale è quindi computazionalmente molto pesante.

Il secondo picco dell'autocorrelazione mi darà l'informazione della periodicità spettrale.

>Questo tipo di calcolo è quello che molto probabilmente fa il nostro sistema percettivo per valutare il pitch di un suono.

Nella valutazione dell'altezza di un suono conta la periodicità del spettrale e non altro.

La quasi-periodicità viene valutata come una armonicità.

Come le campane di orchestra vengono valutate dal nostro sistema percettivo come un'altezza che nello spettro stesso in verità non vi è.

Vi sono dei circuiti che fanno un'operazione di confronto del segnale di una copia di se stesso ritardato.

### `TimeZeroCrossings`

Lo zero crossing conta i passaggi per lo zero del segnale per individuarne le caratteristiche di periodicità.

Supponendo di avere un segnale come una sinusoide e conto i passaggi per lo zero, divido per 2 perchè so che per ogni periodo vi sono 2 passaggi per lo zero, ed avrò ogni periodo della funzione che sto analizzando.

Saprò quale è il periodo della funzione che sto analizzando.

Per il trombone la periodicità del segnale è quella evidenziata ma vi sono delle armoniche superiori:
![/img/trombone1](/img/trombone1.png)

Vi sono delle tecniche elaborate che servono per evitare di prendere le armoniche superiori, individuando la fondamentale anche partendo da un'analisi dei passaggi per lo zero.

Usata molto nelle applicazioni in tempo reale, poichè veloce computazionalmente.

Le schede grafiche sono delle nipotine di DSP.

(analisi di altri animali è interessante e non la tigre: elefanti, cetaceii; diviene interessante il profilo di modulazione del pitch)

### Analisi del pitch nel parlato

![/img/pa](/img/pa.png)

Osserviamo che la correlazione fra diversi descrittori funziona molto ed i picchi maggiori corrispondono a consonanti.

Il tasso di variazione dello spettro è molto diverso tra consonanti e vocali, segmento il parlato e poi applico sulle vocali il pitch.

Quando devo fare il riconoscimento del parlato non è il pitch l'informazione che mi interessa.

## Cepstrum

Analisi importante in diverse applicazioni...

### Come si forma il segnale del parlato?

Abbiamo la glottide che produce il segnale a dente di sega, ovvero la glottide.

Il suono è una forma d'onda che assomiglia ad un dente di sega: [visibile qui](/img/http://hyperphysics.phy-astr.gsu.edu/hbase/Music/vocres.html#c4)

![/img/voc](/img/voc.png)

Il segnale che esce fuori è la convoluzione tra il segnale a dente di sega e i tratti nasali e orali.

Vengono fuori degli spettri con risultanti...

Le gobbe dello spettro sono le formanti che ci danno il gesto e la postura degli articolatori vocali, ed attraverso quella postura riconosciamo il parlato.

In ciò non è tanto il pitch che ci interessa, ma il profilo di inviluppo spettrale, la forma dello spettro.

Anche lo spettro è quindi un segnale.

Nel caso del segnale delle corde vocali avremo quindi un segnale armonico e periodico(dente di sega), anche lo spettro sarà periodico.

Il segnale di inviluppo spettrale sarà anch'esso armonico ma con sviluppo piú lento.

>Abbiamo due segnali che si mescolano nel tempo con un tasso di variabilità diverso.

L'inviluppo spettrale, rispetto al segnale glottale è molto piú lento.

### Che cos'è il CEPSTRUM?

CEPSTRUM -> SPEC TRUM

QUEFRENCY -> FREQ ENCY

LIFTERING -> FILTER ING

RHAMONIC -> RHAM ONIC

Se abbiamo un segnale glottale, poi il filtro dei risonanti vocali e il segnale risultante, da un punto di vista del tempo abbiamo una convoluzione e dal punto di vista della frequenza abbiamo una moltiplicazione:
![/img/s](/img/s.png)

Prendendo il logaritmo del prodotto vediamo che possiamo avere la somma dei due logaritmi:
![/img/l](/img/l.png)

Trasformo quindi un prodotto in una somma...

Delle parti sommate rispetto ad esse moltiplicate è un qualcosa di diverso.

Se prendo la DFT inversa del logaritmo, avendo un operatore lineare avrò quindi:
![/img/dft](/img/dft.png)

Faccio lo spettro del logaritmo dello spettro...

>Esso mi dirà la variabilità ed il contenuto in termini armonici e periodici dello spettro, tratta lo spettro come se fosse un segnale anche lui.

Otterrò delle informazioni sulla variabilità ed il contenuto armonico e periodico di ogni parziale.

Da un punto di vista fisico facendo una DFT al contrario tornerei al domino del tempo, ma non sarà ne il dominio del tempo, ne il dominio della frequenza.

Parleremo sempre di tempo in secondi, ma parleremo di Quefrenze.

Quando faccio il logaritmo dello spettro, analizzo il contenuto frequenziale dello spettro; il fatto di aver fatto il logaritmo mi comporta che io possa separare i sengali glottali e quelle del filtro del tratto vocale e troverò le due componenti separate.

La parte del segnale glottale, poichè piú veloce, si troverà sulla parte destra del grafico, quella del filtro si troverà sulla sinistra.

Tagliando la parte di destra dello spettro, avrò la forma dello spettro e riconoscerò lo strumento o le caratteristiche di una voce dalle formanti vocaliche.

![/img/voca](/img/voca.png)

Se riuscissi a separare la parte dello spettro veloce e lenta avrei delle informazioni in piú.

Il CEPSTRUM è un tipo di analisi che mi consente di isolare e separare la componente di variazione veloce dello spettro, dalla componente di variazione lenta dello spettro.

Quando ho separato queste due componenti, posso fare ciò che mi pare.

Usando dei filtri risonanti che sono impostati come le risonanze umane, riconoscerò una voce, altrimenti non verrà fuori una voce ma qualcos'altro.

### MFCC

Essi sono una variazione del CEPSTRUM:
![/img/mfcc](/img/mfcc.png)

Prima di fare la trasformata, faccio un Mel-scaling, ovvero faccio un riscalamento su scala Mel, essa è una scala di percezione dell'altezza come percepita dagli umani.

Avremo quindi uno spettro che rispecchia le caratteristiche della percezione umana dell'altezza.

La trasformata coseno discreta usa solo la parte del coseno della DFT ovvero la sola parte reale.

Ciò che mi viene fuori dalla catena sono gli MFFC.

Il segnale viene fatto a fettine e passato in un banco di filtri e pochi di questi coefficienti mi danno informazioni sulla forma dello spettro, ovvero sono in grado custodire e mostrare la forma dello spettro, che mi permette di fare molte cose, come: riconoscere uno strumento, etc...

Con poche informazioni riesco ad avere una codifica abbastanza fedele e rigorsa di quel timbro.

(Esempio di Serra degli algoritmi di Deep Learning)

Avendo 50 descrittori, per mettere insieme tutte queste informazioni lavoriamo in uno spazio a 50 dimensioni, avendo un descrittore vettoriale come gli MFCC, che ci consente di ottenere una descrizione vettoriale di quel timbro, ci fa svoltare...

Gli MFCC sono presenti nella libreria di Emmanuel Jourdan [zsa.descriptor](/img/http://www.e--j.com/index.php/what-is-zsa-descriptors/)

Il numero di bande mi darà il numero di coefficienti MFCC, nell'esempio in figura abbiamo 13 valori:
![/img/mf](/img/mf.png)

Si è visto che 13 coefficienti sono un ottimo numero di coefficienti per avere discrezione tra due timbri diversi.

## Lavoro d'esame

Preparare una relazione prendendo uno stesso strumento con stessa nota e diverse eccitazioni o piú strumenti, o prendere uno strumento reale o sintetizzato e fare un confronto basandosi su dei descrittori.

Un altro descrittore lo studieremo da soli ed oltre a studiarlo e cercare di capirlo fare un'analisi basata sui descrittori che abbiamo visto, facendo uno script e tirando fuori una visualizzazione commentando il risultato a cui si è giunti con i descrittori usati.
