# Appunti della lezione di venerdì 17 settembre 2021

## Visione delle analisi dei descrittori sui vari strumenti realizzate da @DavideTedesco

Per le analisi dei descrittori riportate di seguito è stato utilizzato il seguente script:
```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path1 = "bass_clarinet.wav"
path2 = "guitar.wav"
path3 = "tam_tam.wav"
path4 = "tuba.wav"
path5 = "viola_glissando.wav"

[fs, x] = pyACA.ToolReadAudio(path1)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=256)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=256)

#plottiamo

plt.subplot(3,1,1) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(time,x)
plt.xlabel("Tempo")
plt.ylabel("Segnale")

plt.subplot(3,1,2) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t, np.log(dsf))
plt.xlabel("Tempo")
plt.ylabel("Spectral Flattness")

plt.subplot(3,1,3) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t,dsx)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flux")

plt.show()
```

I file audio sono invece reperibili [qui](/audio_files).

Le analisi riportate di seguito puntano ad osservare gli stessi descrittori ma con diverse tipologie di strumenti, è importante sottolineare che esse sono state realizzate a puro scopo esemplificativo e in un contesto di maggiore approfondimento si consiglia l'uso ed il confronto di descrittori scelti ad hoc per ogni suono.

### Clarinetto Basso

Osserviamo in questo primo esempio, come la riproduzione logaritmica della piattezza spettrale, è forse poco indicata, poichè non fa risaltare delle informazioni che forse potrebbero essere contenute in varie regioni del campione.

![bass_clarinet.png](img/bass_clarinet.png)

### Chitarra

Per la chitarra notiamo come sarebbe stato utile il descrittore della brightness, per ottenere informazioni sullo spostamento progressivo delle armoniche.

È importante notare che l'analisi umana ci dovrebbe mettere in evidenza proprio le differenze di analisi fra gli strumenti e guidarci verso l'utilizzo di descrittori diversi. (?)

![guitar.png](img/guitar.png)

### Tam tam

La Spectral Flatness rimane sempre molto alta sul tam-tam, poiché il suono é sempre molto vicino al rumore, ciò è dovuto anche a dei meccanismi di ricircolo dell'energia in cui lo spettro viene modificato.

![tam_tam.png](img/tam_tam.png)

### Tuba

Osserviamo come sia molto probabile che nel grafico del flusso spettrale vi siano degli artefatti che si possono rimuovere cambiando la risoluzione della finestra che osserviamo per osservare i fenomeni che avvengono con una tempo varianza più elevata.

![tuba.png](img/tuba.png)

### Viola
Osserviamo in questa immagine come il contributo del rumore sia presente nelle parti iniziali e finali del segnale, ciò indica come il tallone e la punta dell'arco siano produttori di segnale tendente al rumore, è visibile ciò nel grafico della piattezza spettrle che ha picchi proprio sulla parte inziale e finale del segnale.

Essendo un glissando e non un suono continuo, come i campioni precedenti, vediamo come il flusso spettrale vari molto nel tempo.

![viola_glissando.png](img/viola_glissando.png)

## Correzione dello script della scorsa lezione

Per la visualizzazione dei descrittori, durante la scorsa lezione avevamo osservato come la piattezza spettrale veniva riportata schiacciata, rendendola non comprensibile(la osserviamo in rosso in quest'immagine):
![mod.png](img/mod.png)

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo
#tratto da https://matplotlib.org/devdocs/gallery/subplots_axes_and_figures/two_scales.html

fig, ax0 = plt.subplots() #sovrappore grafici con assi y diversi

#plot del Segnale
color = 'silver'
ax0.set_ylim(-1,1)
ax0.set_xlabel('Tempo')
ax0.set_ylabel('Segnale', color=color)
ax0.plot(time, x, color=color)
ax0.tick_params(axis='y', labelcolor=color)

ax1 = ax0.twinx()  # instantiate a second axes that shares the same x-axis

#plot della Spectral Flatness
color = 'tab:red'
ax1.set_ylim(-6,0)
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Scala SpectralFlatness', color=color)
ax1.plot(t, np.log(dsf), color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

#plot dello Spectral Flux
color = 'tab:blue'
ax2.set_ylabel('Scala SpectralFlux', color=color)  # we already handled the x-label with ax1
ax2.plot(t, dsx, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()
```
Modificando la parte finale, ed usando opportunamento il metodo `subplots()` della libreria `matplotlib`, riusciremo a rendere gli assi _y_ indipendenti, riuscendo a produrre un grafico che possa farci visualizzare ogni descrittore(ed il segnale) nella propria scala, ottenendo quindi:
![col.png](img/col.png)



## Esercizio da svolgere

Partendo da un campione di formato wav mono:
- tagliare il file(eliminando le regioni di silenzio)
- normalizzarlo
- graficare in sovraimpressione la RMS in un primo grafico con il segnale dietro
- graficare un altro descrittore tra i descrittori visti

## Altri descrittori

Descrittori come gli MFCC sono molto potenti ma danno un forte indirizzo sulla distinguibilità dei segnali.
