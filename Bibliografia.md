# Bibliografia del corso

- Capitoli 2, 3 e 4 del testo _An Introduction to Audio Content Analysis Applications in Signal Processing and Music Informatics_ di Alexander Lerch
- Tesi di dottorato di Tae Hong Park dal titolo _Towards Automatic Musical Instrument Timbre Recognition_
- Captioli selezionati dal testo _MPEG-7 Audio and Beyond_ di Kim, Moreau e Sikora
- script realizzati a lezione presenti [qui](/sorgenti-script-lezioni)
