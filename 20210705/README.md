# Appunti della lezione di lunedì 5 luglio 2021

## Nelle puntata precedente...
Avevamo parlato di cosa volesse dire estrarre dei descrittori nel dominio del tempo e nel dominio della frequenza, ed avevamo visto il tema della risoluzione dell'analisi nel dominio tempo-frequenza.

Abbiamo quindi capito i limiti della STFT ed abbiamo realizzato il primo esempio del calcolo e visualizzazione di uno spettrogramma di un file audio.

È importante ricordare che: il tipo di analisi che abbiamo usato per ricavarci dei descrittori si basa sulla segmentazione del file audio in finestre, chiamate frame, ciò porta a definire due matrici:
- una nel dominio del tempo
- l'altra nel dominio della frequenza

Esse fanno si di avere descrittori nel dominio del tempo e descrittori nel dominio della frequenza(dominio di Fourier).

Usiamo delle matrici perchè facendo a fettine il segnale, non avremo piú una struttura dati lineare come un array, ma avremo una riorganizzazione il segnale in cui:

- sulle righe in cui troveremo i frame
- sulle colonne troveremo i time stamps, ovvero l'inizio o punto iniziale di ogni frame

Nel dominio della frequenza avremo un analoga struttura dati, ma con DFT di ogni finestra analizzata in ogni spazio della matrice.

>Abbiamo visto che nel calcolo della DFT si inviluppa ogni finestra per specifica funzione, per evitare ringing ai bordi della finestra(nel caso fosse quadrata); inoltre non si realizzano finestre secche per tagliare il segnale ma si cercherà sempre di avere dei tagli di porzioni delle finestre di circa il 50% delle finestre, per far si che parte di esse si sovrappongano.

Dal punto di vista matematico questo tipo di taglio con finestre sovrapposte permette di ricavare una ricostruzione del segnale sotto certe condizioni, procedimento denominato _Overlap and add_.

## Un descrittore nel dominio del tempo: RMS

Lo scopo della lezione di oggi sarà ricavare la matrice delle frame temporali e da essa ricavarne gli elementi RMS di un segnale.

>Ciò ci è utile per capire come costruire la matrice delle frame temporali e per capire la libreria che useremo in seguito.

Tutte le componenti necessarie per realizzare ciò, le potremmo infatti scrivere a mano, ma abbiamo anche una libreria già pronta o comunque per implementare da zero si potrà utilizzare il corso di Xavier Serra su Coursera.

Useremo noi delle funzioni della libreria `signal` di `scipy` che ci fornisce la STFT già pronta.

Ora useremo un'altra libreria che si trova in un altro luogo e che fa riferimento al [sito](https://www.audiocontentanalysis.org/) corrispondente anche ad un [libro](https://www.wiley.com/WileyCDA/WileyTitle/productCd-111826682X,descCd-buy.html) che parte dai descrittori di basso livello ed arriva ai descrittori di alto livello.

Ed al seguente [repository](https://github.com/alexanderlerch/pyACA) possiamo trovare del codice e scripts implementati da Alexander Lerch, autore del libro sopra menzionato.

## Realizzazione dello script RMS

Decisa la grandezza della finestra e l'`hopSize`, consideriamo che avremo sempre un avanzo alla fine perchè alcuni campioni ce li perderemo...

La versione in `pyACA` taglia brutalmente la parte finale del file; per evitare il taglio della parte finale del file, realizziamo uno _zero padding_ del segnale alla fine per far si che quando lo tagliamo a fettine comprenderemo anche il segnale utile alla fine, senza avere tagli artificiosi.

Realizziamo uno _zero padding_ sempre quando si ricava la matrice delle frame frequenziali, quando abbiamo frame che non hanno potenze di due, possiamo aggiustarci la frame con numero di campioni che non è una potenza di due; capendo quale sia la potenza di due immediatamente piú grande e aggiungendo un buffer della grandezza necessaria.

>Il modo in cui il campione viene copiato nella frame di analisi è elaborato e bisogna spaccare il campione in modi diversi per avere la coerenza di fase.

Nel nostro caso **non ci ricaviamo la matrice delle frame frequenziali ma delle frame temporali**, ciò sarà importante per capire i passaggi successivi.

La funzione per il padding in `numpy` è denominata `.pad()`.

la proprietà `.shape[0]` restituisce il numero di elementi di una matrice, e quindi scrivendo `.shape[0]` avremo il numero di righe.


Osserviamo il codice completo(parte di esso è derivata dal codice della lezione precedente):
```
import numpy as np
import sys
import pyACA
import matplotlib.pyplot as plt
from scipy.io.wavfile import read
from funzioni import rms

#leggiamo un file audio(file audio mono)
[fs, x] = read("daniel.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#generiamo la matrice dei frame temporali(segmentazione del campione nel dominio del tempo)
#lunghezza della frame di analisi in campioni
frameLength = 1000
#hopSize in campioni -> di quanto ci spostiamo in ogni finestra di analisi, mentre l'overlap e' il complemento dell'hopSize
hopSize = 500

#zero padding alla fine del file(non necessario in questo caso)
x = np.pad(x, (0,frameLength), 'constant', constant_values=(0,0))
print("\ndimensione del file audio post padding: ", np.size(x))

#costruzione della matrice delle frame temporali
#(array, dimensione, hopSize)
xFrames = pyACA.ToolBlockAudio(x, frameLength, hopSize)
#numero di frame di analisi
nFrames = xFrames.shape[0]
print("\ndimensioni della matrice delle frame temporali: ", xFrames.shape)

#costruzione della base tempi(esso e' un array con istanti di tempo corrispondenti alle varie frame di analisi)
#per avere i valori dell'asse x in unita' temporali
t = (np.arange(0, nFrames)*hopSize + (frameLength/2))/fs

#estrarre l'RMS
RMS = rms(xFrames)
#RMS =20*np.log(rms(xFrames))

print("\ndimensioni del vettore RMS: ", RMS.shape)

#plot
plt.subplot(2,1,1)#ci interessa il primo subplot dove mettiamo il segnale
plt.grid()
plt.plot(x)
plt.ylabel('segnale')
plt.xlabel('Tempo')

plt.subplot(2,1,2)#ci interessa il secondo subplot dove mettiamo il valore RMS
plt.grid()
plt.plot(t, RMS)
plt.ylabel('RMS')
plt.xlabel('Tempo')

plt.show()
```
Abbiamo quindi ricavato un descrittore scalare temporale.


Osserviamo che sopra vi è riportato una rappresentazione della forma d'onda del segnale, mentre sotto vi è il descrittore RMS:
![img](img/rms.png)

Sappiamo che vi sono anche descrittori vettoriali come lo spettrogramma che per ogni analisi permettono di ricavare una serie di elementi che sono i moduli della DFT.

Altri descrittori vettoriali sono quindi quelli ricavati da una DFT con poi il calcolo delle parziali di un segnale oltre una certa soglia; per ogni frame si avrà un certo numero di parziali con la loro posizione e la loro ampiezza, avremmo quindi un certo numero di descrittori di parziali vettoriali.

>La differenza fra i descrittori scalari e vettoriali, è che per quelli vettoriali si deve usare un [diagramma waterfall](https://www.roomeqwizard.com/help/help_en-GB/html/graph_waterfall.html) o dei colori per permettere di visualizzare tutte le informazioni ricavate.

## Cosa fare con le due matrici e come usare i vari descrittori?

Supponiamo di avere la descrizione del segnale in diversi tipi di descrittori.

Supponendo di avere un segnale diviso in due matrici, come il dominio del tempo e della frequenza, da queste matrici ci possiamo ricavare dei descrittori. Facendo ciò possiamo avere dei vettori molto grandi, con ad esempio 200 dimensioni che descrivono il segnale interno, in termini delle 200 variabili come descrittori o frammenti di descrittori.

>Un elemento a due dimensioni è un punto nel piano, un elemento in 200 dimensioni è molto difficile da rappresentare, come affrontato nel [seguente articolo](https://towardsdatascience.com/the-art-of-effective-visualization-of-multi-dimensional-data-6c7202990c57).

Immaginando che un timbro ed una nota suonata da uno strumento equivale ad una successione di punti in uno spazio a 200 dimensioni e rappresentando idealmente questa successione di punti potrei quindi definire una traiettoria.

Potrò quindi in qualche modo ricondurre in maniera univoca una traiettoria a quel tipo di strumento e timbro in un modo specifico, in modo da poter riconoscere quel punto(o meglio quella sfera) nello spazio multi-dimensionale.

>Il computer potrebbe essere in grado di riconoscere la successione di punti descritti in uno spazio generalizzato, ma noi non riusciremmo a visualizzarlo chiaramente.

Ma sappiamo che:
- possiamo definire la distanza fra due punti in uno spazio in quante dimensioni vogliamo
- possiamo analizzare una nuova traiettoria cercando di capire cosa sia, ed a cosa appartenga
- possiamo avere ad esempio un riconoscimento del timbro descritto in percentuale

Tutto ció che è in mezzo tra il riconoscimento del timbro e tutto ciò che è descritto da parte nostra viene chiamata nel Machine Learning si chiama "**classificazione**".

Dopo aver inserito una serie campioni abbiamo una macchina in grado di classificarli.

>Nel nostro caso quindi, prendere una singola frame di un suono non ha senso, ma dovremmo avere una evoluzione del suono che da caratteristiche ad un timbro e lo identifica.

Noi non sappiamo di fatto quali sono i descrittori che usa il nostro sistema cognitivo.

>Ora si capisce perchè il timbro è la proprietà piú complessa che si possa descrivere, poichè è un insieme di proprietà cognitive che si riferiscono alla percezione del timbro stesso.

Fin da piccoli realizziamo un apprendimento che lega delle mappe cognitive.

>L'ascolto di uno strumento diviene come una scarica elettrica nel tempo con delle mappe spaziali che si immagazzinano nella nostra storia, avendo un confronto velocissimo e molto efficiente con tutto il catalogo di mappe nella nostra testa riusciamo poi in un secondo momento a riconoscere dei timbri.

Il Machine Learning e le Intelligenze Artificiali cercano quindi di ricostruire un modello di timbro, e cercano di imitare i descrittori che abbiamo cablati dentro di noi.

Il fatto che i descrittori funzionino sta nel fatto che devo scegliere come ricavare i descrittori.

A questo punto sorgono molte domande come:
- quali sono i descrittori che devo scegliere?
- quali segnali scelgo per addestrare gli algoritmi?
- quali algoritmi scelgo da addestra?
- e molte altre

Abbiamo tante domande da porci per capire i descrittori quali devono essere e come e cosa devono apprendere le macchine.

## Descrittori di piú alto livello

Quando descriviamo un genere musicale, lo descriviamo in base a dei descrittori precisi, essi sono di fatto dei descrittori di alto livello.

La MIR(Music Information Retrival) cerca di definire descrittori di livelli diversi per arrivare a realizzare analizzatori di descrizione di piú alto livello.

### Il problema della "corenza dello stile"

Un compositore si puó porre la domanda: cosa descrive il mio stile e la corenza che ho nella mia scrittura a tutto il mio passato ed il mio futuro?

In breve il compositore si sta chiedendo: cercando di variare un qualcosa, cosa è una costante nel mio stile?

Già quando si ha una musica tonale del 600 con uno stile e si ha ogni autore che si muove in un certo set di regole possiamo riconoscere delle differenze, avremmo quindi una pluralità di stili molto diversi.

>Se riuscissi ad accumulare molti descrittori che mi danno molte informazioni, potremmo costruire una composizione basandoci sulle informazioni accumulate, ma ciò sarà nello stile prefissato?

## Riflessioni sull'arte e l'intelligenza

**L'intelligenza, la capacità e la probabilità di riconoscere la bellezza non dipende da diagrammi cartesiani ma ciò che è cablato nella mente e nel corpo.**

Tra le domande che ci poniamo vi è: cosa vuol dire godere per un'opera d'arte?

Tutto ciò che sta venendo fuori è che muovendoci stiamo pensando, e che i movimenti ci servono proprio come elementi di astrazione.

Analizzare i sistemi complessi e cosa sono, puó avvicinarci a capire come funziona il sistema umano.

>Qualche anno fa parlando di intelligenze artificiali si parlava di: _Sistemi esperti_ .
---
## Articoli consigliati:

- [Come le neuroscienze spiegano le emozioni che proviamo davanti a un'opera d'arte](https://www.wired.it/scienza/lab/2021/06/25/neuroscienze-emozioni-arte-gallese-spoleto/)
