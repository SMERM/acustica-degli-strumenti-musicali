import pyACA
import matplotlib.pyplot as plt
import numpy as np

# read audio file
cPath = "speech-male.wav"
#cPath = "sine1000.wav"
#cPath = "oboe.wav"
#cPath = "Piano.mf.A4.mono.wav"
#cPath = "trombone.mf.G2.wav"


[f_s,afAudioData] = pyACA.ToolReadAudio(cPath)

# rimozione del valore medio
afAudioData = afAudioData - np.mean(afAudioData)
# normalizzazione
afAudioData = afAudioData / np.max(abs(afAudioData))
# costruzione base tempi a partire dall'ordine dei campioni
time = np.arange(0,np.size(afAudioData)) / f_s

# calcolo MFCC
N = 1024
hop = 512

[MFCC,t] = pyACA.computeFeature("SpectralMfccs", afAudioData, f_s, iBlockLength=N, iHopLength=hop)

plt.pcolormesh(MFCC, cmap='nipy_spectral')
plt.title('MFCC')
plt.ylabel('Mel coefficients')
plt.xlabel('Time [frame]')


plt.show()
