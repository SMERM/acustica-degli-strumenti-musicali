#funzioni per il calcolo dell'RMS e non solo
"""
Feature extraction of RMS - Root Mean Square

Args:
    m: matrix of temporal audio frames

Returns:
    1D array containing RMS value for each temporal frame
"""

import numpy as np

def rms (m):
    #square each element of the matrix
    m = np.square(m)
    #mean(medie relative ad ogni frame)
    m = np.mean(m, axis=1) #media per riga
    #root(ne realizziamo la radice quadrata)
    m = np.sqrt(m)

    return m
