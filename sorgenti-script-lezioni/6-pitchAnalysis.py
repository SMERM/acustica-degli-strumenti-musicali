import pyACA
import matplotlib.pyplot as plt
import numpy as np

# read audio file
cPath = "speech-male.wav"
#cPath = "sine1000.wav"
#cPath = "oboe.wav"
#cPath = "Piano.mf.A4.mono.wav"
#cPath = "trombone.mf.G2.wav"
#cPath = "roar_tiger_mono.wav"


[f_s,afAudioData] = pyACA.ToolReadAudio(cPath)

# rimozione del valore medio
afAudioData = afAudioData - np.mean(afAudioData)
# normalizzazione
afAudioData = afAudioData / np.max(abs(afAudioData))
# costruzione base tempi a partire dall'ordine dei campioni
time = np.arange(0,np.size(afAudioData)) / f_s

# calcolo dei descrittori
N = 1024
hopSize = 512
# RMS
[vtRMS,tRMS] = pyACA.computeFeature("TimeRms", afAudioData, f_s, iBlockLength=N, iHopLength=hopSize)
# Spectral Centroid
[vsc,t] = pyACA.computeFeature("SpectralCentroid", afAudioData, f_s, iBlockLength=N, iHopLength=hopSize)
# Pitch Tracker: per info sui tracker disponibili guardare il file computePitch.py di pyACA
[vP,tP] = pyACA.computePitch("TimeAcf", afAudioData, f_s, iBlockLength=N, iHopLength=hopSize)


# mi ricavo gli handler della chiamata a subplots (2 righe, una colonna)
# axs contiene una lista di oggetti che fanno riferimento agli assi
fig, axs = plt.subplots(3,1)

# configuro il primo grafico attraverso il primo oggetto della lista axs
color = 'silver'
axs[0].set_xlabel('Tempo')
axs[0].set_ylabel('segnale', color=color)
axs[0].tick_params(axis='y', labelcolor=color)
axs[0].plot(time, afAudioData, color = color)

# clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
ax2 = axs[0].twinx()
# plot RMS
color = 'tab:red'
ax2.set_ylabel('RMS', color=color)
ax2.tick_params(axis='y', labelcolor=color)
ax2.plot(tRMS, vtRMS, color = color)

# configuro il grafico della seconda finestra
# plot Spectral Centroid
color = 'tab:blue'
axs[1].set_ylabel('S. Centroid', color=color)
axs[1].tick_params(axis='y', labelcolor=color)
axs[1].plot(t, vsc, color = color)

# configuro il grafico della terza finestra
# plot pitch
color = 'tab:green'
axs[2].set_ylabel('Pitch', color=color)
axs[2].tick_params(axis='y', labelcolor=color)
axs[2].plot(tP, vP, color = color)

plt.show()
