import numpy as np
import sys
import pyACA
import matplotlib.pyplot as plt
from scipy.io.wavfile import read
from funzioni import rms

#leggiamo un file audio(file audio mono)
#[fs, x] = read("daniel.wav")
[fs, x] = read("oboe.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#generiamo la matrice dei frame temporali(segmentazione del campione nel dominio del tempo)
#lunghezza della frame di analisi in campioni
frameLength = 1000
#hopSize in campioni -> di quanto ci spostiamo in ogni finestra di analisi, mentre l'overlap e' il complemento dell'hopSize
hopSize = 500

#zero padding alla fine del file(non necessario in questo caso)
x = np.pad(x, (0,frameLength), 'constant', constant_values=(0,0))
print("\ndimensione del file audio post padding: ", np.size(x))

#costruzione della matrice delle frame temporali
#(array, dimensione, hopSize)
xFrames = pyACA.ToolBlockAudio(x, frameLength, hopSize)
#numero di frame di analisi
nFrames = xFrames.shape[0]
print("\ndimensioni della matrice delle frame temporali: ", xFrames.shape)

#costruzione della base tempi(esso e' un array con istanti di tempo corrispondenti alle varie frame di analisi)
#per avere i valori dell'asse x in unita' temporali
t = (np.arange(0, nFrames)*hopSize + (frameLength/2))/fs

#estrarre l'RMS
RMS = rms(xFrames) #scala lineare
RMS_log = 20*np.log(rms(xFrames)) #scala logaritmica

print("\ndimensioni del vettore RMS: ", RMS.shape)

#plot
plt.subplot(2,1,1)#ci interessa il primo subplot dove mettiamo il segnale
plt.grid()
plt.plot(x)
plt.ylabel('segnale')
plt.xlabel('Tempo')

plt.subplot(2,1,2)#ci interessa il secondo subplot dove mettiamo il valore RMS
plt.grid()
plt.plot(t, RMS)
plt.ylabel('RMS')
plt.xlabel('Tempo')

plt.show()
