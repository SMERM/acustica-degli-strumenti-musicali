"""
script per dimostrare il significato del cepstrum usando un'analogia col segnale vocale
nel quale un segnale a dente di sega viene filtrato con una funzione formante che ne
sagoma il profilo spettrale.

"""

import numpy as np
from scipy import signal, fft, ifft
import matplotlib.pyplot as plt

N = 4096
t = np.linspace(0, 1, N)

# generazione del segnale glottale
fg = 128 # numero di periodi in N del segnale glottale
g = signal.sawtooth(2 * np.pi * fg * t)
G = np.fft.fft(g) # DFT del segnale glottale

# generazione dell'inviluppo delle "formanti"
# mi costruisco una funzione nel dominio di Fourier che filtra il segnale glottale (spettro delle fasi nullo)
f = np.arange(0,N)
fif = 2.5 # numero di periodi della funzione formante su N (comprese frequenze negative)
FORM = np.abs(np.cos(2 * np.pi * fif * f / N))
form = np.real(np.fft.ifft(FORM)) # DFT inversa: riporto la funzione nel dominio del tempo

# mi ricavo il segnale di uscita come convoluzione nel dominio della frequenza (prodotto tra le due DFT)
VOC = np.abs(G * FORM)

# log spectrum
VOC = VOC + 1e-10 # per evitare warning sul 'division by zero'
lVOC = np.log(VOC)

# cepstrum: ifft del log spectrum
cepstrum = np.real(np.fft.ifft(lVOC))

plt.figure(1)
plt.subplot(2,1,1)
plt.title('segnale glottale')
plt.plot(t, g)
plt.subplot(2,1,2)
plt.title('spettro dei moduli del segnale glottale')
plt.plot(np.abs(G))

plt.figure(2)
plt.subplot(2,1,1)
plt.title('segnale filtro formantico nel tempo')
plt.plot(form)
plt.subplot(2,1,2)
plt.title('spettro dei moduli del filtro formantico')
plt.plot(f, FORM)

plt.figure(3)
plt.subplot(3,1,1)
plt.title('segnale "vocale" finale')
plt.plot(VOC)
plt.subplot(3,1,2)
plt.title('log dello spettro')
plt.plot(lVOC)
plt.subplot(3,1,3)
plt.title('cepstrum')
plt.plot(np.abs(cepstrum))

plt.show()
