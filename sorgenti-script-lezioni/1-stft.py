import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

# leggo un file audio (mono)
[fs, x] = read("daniel.wav")
print("SR:", fs, "\ndimensione del file audio: ", np.size(x))

# normalizzo il file
x = x / np.max(abs(x))

# calcolo la matrice stft
f, t, STFT = signal.stft(x, fs, nperseg=1024, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

# plot
plt.figure(1)
plt.ylabel('segnale')
plt.grid()
plt.plot(x)

plt.figure(2)
plt.pcolormesh(t, f, np.abs(STFT), vmin=0, vmax=np.max(abs(STFT)), shading='auto')
plt.xlabel('Tempo [s]')
plt.ylabel('Frequenza [Hz]')
plt.show()
