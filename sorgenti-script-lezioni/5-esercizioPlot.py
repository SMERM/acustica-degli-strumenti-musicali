import pyACA
import matplotlib.pyplot as plt
import numpy as np

# read audio file
#cPath = "speech-male.wav"
#cPath = "sine1000.wav"
#cPath = "oboe.wav"
#cPath = "Piano.mf.A4.mono.wav"
cPath = "trombone.mf.G2.wav"


[f_s,afAudioData] = pyACA.ToolReadAudio(cPath)

# rimozione del valore medio
afAudioData = afAudioData - np.mean(afAudioData)
# normalizzazione
afAudioData = afAudioData / np.max(abs(afAudioData))
# costruzione base tempi a partire dall'ordine dei campioni
time = np.arange(0,np.size(afAudioData)) / f_s

[vtRMS,tRMS] = pyACA.computeFeature("TimeRms", afAudioData, f_s, iBlockLength=1024, iHopLength=512)
[vsc,t] = pyACA.computeFeature("SpectralCentroid", afAudioData, f_s, iBlockLength=1024, iHopLength=512)

# mi ricavo gli handler della chiamata a subplots (2 righe, una colonna)
# axs contiene una lista di oggetti che fanno riferimento agli assi
fig, axs = plt.subplots(2,1)

# configuro il primo grafico attraverso il primo oggetto della lista axs
color = 'silver'
axs[0].grid()
axs[0].set_xlabel('Tempo')
axs[0].set_ylabel('segnale', color=color)
axs[0].tick_params(axis='y', labelcolor=color)
axs[0].plot(time, afAudioData, color = color)

# clono l'oggetto axs[0] per gestire un secondo grafico nella stessa finestra del primo
ax2 = axs[0].twinx()
# plot RMS
color = 'tab:red'
ax2.grid(color = color)
ax2.set_ylabel('RMS', color=color)
ax2.tick_params(axis='y', labelcolor=color)
ax2.plot(tRMS, vtRMS, color = color)

# configuro il grafico della seconda finestra
# plot S. centroid
color = 'tab:green'
axs[1].grid()
axs[1].set_xlabel('Tempo')
axs[1].set_ylabel('S. Centroid', color=color)
axs[1].tick_params(axis='y', labelcolor=color)
axs[1].plot(t, vsc, color = color)

plt.show()
