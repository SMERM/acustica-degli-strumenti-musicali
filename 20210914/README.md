# Appunti della lezione di martedì 14 settembre 2021

I descrittori analizzati in questa lezione hanno dei rispettivi metodi nella libreria di Alexander Lerch `pyACA`.

## Spectral flatness o _piattezza spettrale_

Essa è il rapporto tra la media geometrica dei moduli dei bin e la media aritmetica dei moduli del bin.

>Questo descrittore da quindi informazioni sulla presenza di parziali armoniche nel segnale.

Ricordiamo ovviamente, che una media geometrica da 0 se anche solo una parziale è nulla.

### Media geometrica
Essa si calcola attraverso i prodotti, con il prodotto di tutti i moduli e se ne fa la radice(non quadrata) di tutti i valori.

Essa è così descritta:
![img/mg.png](img/mg.png)

### Media aritmetica

Essa è una somma di tutti i valori e la si divide per il numero dei valori che si sono sommati.

### Cosa indica la Spectral Flatness?

Osserviamo quindi che la media geometrica a differenza di quella aritmetica, è un oggetto matematico molto sensibile alla nullità degli elementi al suo interno.

Di fronte ad uno spettro rigato, ovvero uno spettro formato da tante parziali sovrapposte la piattezza spettrale avrà valori bassi( dobbimamo considerare che un rumore avrà uno spettro complementare a quello rigato).

Avendo uno spettro rumoroso, avremo che la media geometrica e la media aritmetica, tenderanno ad essere uguali portando la frazione quasi ad 1.

Per riconoscere le regioni vocaliche da quelle consonantiche, utilizzeremo infatti questo descrittore, che oscilla tra zero ed uno.

Per i segnali rigati, ovvero quelli vocalici o armonici, avendo un po' di storia del campione, potremo cercare di capire come si potrebbe evolvere successivamente nel tempo.

## Spectral flux o _flusso spettrale_

Esso è un descrittore che ha uno scopo piú dinamico e da un'idea della differenza che c'è tra una fotografia dello spettro ad un certo istante, ovvero ad una certa finestra, e lo stesso spettro alla finestra successiva.

Guardando in sequenza i moduli, (vedendo le fettine dello spettrogramma messi in fila), o vedendo l'animazione dello spettro man mano che il segnale scorre nel tempo, ci possiamo immaginare un descrittore che ci dia la differenza tra una frame e la frame successiva.

Esso ci dirà quindi la dinamica o mobilità spettrale.

Nella fase di attacco avremo quindi una differenza algebrica maggiore e il flusso spettrale indicherà che vi è appunto una grande differenza tra una frame e l'altra.

Questo descrittore utilizza la _norma di secondo ordine_, elevando tutti i bin al quadrato e facendone la radice e poi sommandone tutti i valori.

![img/2norma.png](img/2norma.png)

>Avremo quindi la distanza in termini geometrici cartesiani che vi è fra due punti in uno spazio ideale che ha tante dimensioni quanti sono i punti della finestra.

Notiamo che:
- Se lo spettro si muove poco, i due punti saranno vicini
- Se lo spettro si muove tanto i due punti saranno lontani

Lo Spectral Flux, servirà quindi ad avere un'idea per capire se sono nella fase di attacco di un suono, o nella fase di sustain, in cui lo spettro sarà abbastanza stabile.

## Applicazione pratica dei descrittori appena affrontati

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo
#tratto da https://matplotlib.org/devdocs/gallery/subplots_axes_and_figures/two_scales.html

fig, ax1= plt.subplots()

color = 'tab:green'
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Segnale', color=color)
ax1.plot(time, x, color=color)
ax1.tick_params(axis='y', labelcolor=color)

color = 'tab:red'
ax1.set_xlabel('Tempo')
ax1.set_ylabel('Scala SpectralFlatness', color=color)
ax1.plot(t, dsf, color=color)
ax1.tick_params(axis='y', labelcolor=color)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('Scala SpectralFlux', color=color)  # we already handled the x-label with ax1
ax2.plot(t, dsx, color=color)
ax2.tick_params(axis='y', labelcolor=color)

fig.tight_layout()  # otherwise the right y-label is slightly clipped

plt.show()
```

Da questo script ricaviamo:
![img/mod.png](img/mod.png)

### Plottiamo in modo differente: descrittori e segnale separato

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#genero una base tempi coerente con il segnale analizzato, usando l'istruzione arenge
time = np.arange(0,np.size(x))/fs

#calcolo del descrittore
#centroide spettrale
#[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale
#spectral flattness
[dsf, t] = pyACA.computeFeature("SpectralFlatness",x,fs,iBlockLength=1024, iHopLength=512)
#spectral flux
[dsx, t] = pyACA.computeFeature("SpectralFlux",x,fs,iBlockLength=1024, iHopLength=512)

#plottiamo

plt.subplot(3,1,1) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(time,x)
plt.xlabel("Tempo")
plt.ylabel("Segnale")

plt.subplot(3,1,2) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t, dsf)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flattness")

plt.subplot(3,1,3) #numero di grafici verticali, numero di finestre orizzontali, numero progressivo
plt.grid()
plt.plot(t,dsx)
plt.xlabel("Tempo")
plt.ylabel("Spectral Flux")

plt.show()
```

Ottenendo quindi:
![img/3plot.png](img/3plot.png)

Osserviamo che in questa rappresentazione Spectral Flatness non è molto comprensibile e tutte le informazioni interessanti potrebbero essere contenute nelle creste che si denotano; decidiamo quindi di rappresentare la _piattezza spettrale_ in maniera logaritmica, amplificandone le variazione ciò lo possiamo fare modificando il plot della Flattness in questo modo:
```
plt.subplot(3,1,2)
plt.grid()
plt.plot(t, np.log(dsf))
plt.xlabel("Tempo")
plt.ylabel("Spectral Flattness")
```

Ed ottenendo quindi:
![img/log.png](img/log.png)

### Considerazioni

Osserviamo che all'interno delle oscillazioni dei valori possiamo rintracciare l'alternarsi di consonanti e vocali ed i picchi della flattness sono quelli che corrispondono alle consonati mentre le zone statiche alle vocali.

Lo Spectral Flux, oltre i momenti di silenzio molto marcati, ci indica che abbiamo un segnale con una variabilità elevata, e stiamo praticamente affermando che abbiamo un segnale non stazionario; sappiamo infatti che il parlato è un segnale estremamente tempo-variante e quindi non stazionario.

Le variazioni piú marcate del flusso le notiamo infatti con la transizione tra consonante e vocale.

## Come faccio a segmentare ericonoscere il parlato?
Questo procedimento è realizzato con algoritmi di segmentazione che usano i descrittori appena osservati
.
Google translate passa ad esempio un segnale in:

- un fitro che lo rende a fettine
- ogni fonema viene passato nell'algoritmo di riconoscimento dei fonemi -> supponendo di avere l'elemento piccolo del fonema, se voglio addestrare l'algoritmo a riconoscere il fonema, lo faccio esercitare e gli attacco un etichetta -> avendo imparato a distinguere i fonemi
- un algoritmo che mette insieme i fonemi
- una serie di algoritmi che mettono insieme i risultati e cercano di indovinare quale sia la parola cercando anche di indovinare la parola successiva


### Individuazione dei fonemi
Dopo ho riconosciuto un fonema, ogni descrittore avrà un suo valore per quel fonema, e quei 10 descrittori e valori diverranno come un punto in uno spazio a 10 dimensioni da ora in poi potremmo valutare quale sia la distanza tra un fonema e tutti quelli che abbiamo in memoria e che corrisponderanno al bagaglio di apprendimento.

Prendendo 200 esempi e catalogandoli come un preciso fonema, avrò un insieme di 10 valori per quei fonemi, in uno spazio a 10 dimensioni, avremo quindi un cluster di punti.

Un altro fonema, sul quale abbiamo calcolato gli stessi descrittori, ci darà un altro luogo ed avremo uno spazio a 10 dimensioni occupato da questi fonemi.

Fatta questa classificazione, avremo lo spazio che corrisponderà alla sua esperienza.

Quando introdurremmo un nuovo fonema in questo spazio multi-dimensionale, utilizzeremo i calcoli precedentemente svolti per gli altri fonemi per cercare di individuare il fonema nuovo da riconoscere, individuando la distanza minima del fonema inserita dai cluster precedentemente individuati.

## Analisi dei descrittori degli strumenti: l'oboe

![img/oboe.png](img/oboe.png)

Osservando un campione di una nota lunga suonata su di un oboe notiamo che nella Spectral Flattness abbiamo dei picchi sull'attacco e sulla release(ricordandoci che la flattness è ora visualizzata in modalità logaritmica.).

Vediamo che sul flusso spettrale abbiamo picchi sull'attacco con un assestamento successivo e una piccola gobba nella fase di release, con la perdita progressiva di diverse armoniche(avremo quindi delle variazioni dello spettro).

Osservando le analisi in relazione tempo-frequenza, avremo bisogno di una certa relazione temporale, altrimenti i fenomeni non riusciremo neanche ad osservarli.

È importante quindi osservare con vari tipi di setacci lo stesso segnale, per capire se le analisi che svolgiamo siano artificiose o meno, per varie tipologie di setacci, ora si intendono diverse dimensioni delle finestre e grandezze di sovrapposizione delle stesse.

## Links utili

- [i colori della libreria matplot](https://matplotlib.org/stable/gallery/color/named_colors.html)
