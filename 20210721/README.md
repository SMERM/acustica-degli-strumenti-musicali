# Appunti della lezione di mercoledì 21 luglio 2021

## Nelle puntate precedenti
Avevamo realizzato un primo script per calcolare la matrice STFT e ne avevamo visualizzato lo spettrogramma, osservando come il vettore del campione del segnale viene sezionato ed ogni sezione costituisce una frame di analisi, con risoluzione più o meno grande in frequenza e tempo.

A questo punto abbiamo compreso che le strutture dati con cui stiamo lavorando sono quindi due matrici:

- matrice nel dominio del tempo
- matrice nel dominio della frequenza

Esse hanno come riga la frame i-esima:

- per il tempo -> i campioni
- per la frequenza -> una frame

Esse hanno come colonne la frame i-esima:

- per il tempo -> l'indice dei campioni all'interno della frame
- per la frequenza -> le colonne sono i bin, ovvero la banda di frequenza che rappresenta la risoluzione in frequenza dell'analisi STFT

Dobbiamo ricordarci sempre che _alla base di tutto ció c'è il principio di indeterminazione._

Avevamo quindi capito che lo spettrogramma è la visualizzazione dei moduli degli elementi della matrice nel dominio della frequenza.

Avevamo realizzato un esempio di analisi del segnale di un parlato, che ci porta a capire che per studiare la tempo-varianza di un parlato, facendo speech recognition, mi servirà una risoluzione temporale che mi consente di riconoscere correttamente tutti i fonemi.

È importante notare che in genere nei processi di analisi(anche SPEAR lo realizza), si può **cambiare la risoluzione di analisi nel tempo**, realizzando una risoluzione adattiva, meno di adottare dei sistemi di analisi multi-risoluzione che realizzano un'analisi a diverse risoluzioni contemporaneamente.

Avevamo poi realizzato uno script relativo a un descrittore temporale ovvero quello che ci permetteva di visualizzare i dB RMS.

Capendo che:
>Bisogna essere attenti agli artefatti senza farsi ingannare da pattern strani che si vanno a creare.

## Brightness o centroide spettrale o baricentro spettrale

Per introdurre il prossimo descrittore praticamente spieghiamo brevemente di cosa si tratta; iniziamo col dire che esso è uno dei descrittori piú usati nella MIR.

La **brightness** è un descrittore scalare calcolato nel dominio della frequenza, esso è espresso attraverso un solo numero, ovvero per ogni frame di analisi calcoleremo un solo numero che ci indica questa specifica proprietà del segnale.

### Cos'è la _brightness_ da un punto di vista intuitivo?

Essa è il baricentro dello spettro, ovvero una centroide spettrale, nel dominio della frequenza(da non confondere con la centroide temporale e quindi calcolata nel dominio del tempo).

**La brightness è un descrittore scalare nel dominio della frequenza.**

### Il centroide spettrale, cos'è?

Avendo una matrice della STFT che ha 1024 righe e 3500 frame di analisi, avremo per ogni frame di analisi(ovvero le colonne della matrice), un valore; ed avremo come vettore della brightness un oggetto che avrà 1 riga e 3500 colonne:

![img](img/bri.png)

Avendo quindi un rettangolo alto R e largo L, se dovessimo tenere in equilibro il rettangolo, su un dito, terremmo in equilibro sul dito il rettangolo nel punto medio.

![img](img/re.png)

Il punto medio di un rettangolo sarà quindi L/2, ma quale sarà il punto medio di un triangolo?

![img](img/tr.png)

Pensando che il rettangolo sia un white noise(dal punto di vista dell'occupazione dello spazio frequenziale e d'ampiezza), il suo centro di massa sia la metà, mentre per un spettro triangolare avremo che esso avrà centro di massa spostato.

Quindi per una figura piú complessa(come un segnale complesso) sarà calcolabile un centro di massa o baricentro:

![img](img/f_B=_frac_sum_k_.png)

Con _k_ indice dei bin e _x[k]_ modulo di quell'indice della STFT.

Avremo quindi N elementi della frame, di cui ne facciamo la somma, sopra moltiplicata per k, mentre sotto avremo al sola somma:
![img](img/f_B=_frac_1_cdot.png)

Ciò da un punto di vista dimensionale fa ottenere un indice di bin, che potrebbe essere anche frazionario, ma ció non ci interessa perchè stiamo calcolando la frequenza che corrisponde al baricentro spettrale.

Ció corrisponderà, avendo una rappresentazione dello spettro per mezzo di rettangoli, a moltiplicare l'altezza dei rettangoli per l'indice, per poi dividerli per la somma di tutte le altezze dei rettangoli, realizzando quindi una media pesata. È importante sottolineare che essa è pesata con l'indice dei bin, e ci serve pesata per indicare l'indice preciso in cui individuare il baricentro.

![img](img/see.png)

Nel caso di un rumore, come il white noise avremo invece:
![img](img/ru.png)

E quindi la corrispondente equazione sarebbe:
![img](img/f_B=_frac_1_cdot-1.png)

Ovvero la somma dei primi N numeri diviso N, ma sappiamo che per sapere quanto vale la somma dei primi N numeri, possiamo usare la formula di Gauss.

Quindi tutto ciò va diviso per N, rimanendo poi (N+1)/2, capiremo quindi che il baricentro di un rettangolo uniforme sarà a metà della sua larghezza.
![img](img/f_B=_frac_1_cdot-2.png)

>Da un punto di vista percettivo si dice che l'energia è spostata verso destra o verso sinistra.

L'indice del bin di fatto rappresenta quindi una banda e scegliendo un frame di 1024 campioni, un bin rappresenterà 44100/1024.

### Il centroide temporale

Essa è calcolata sui campioni nel tempo, ovvero è un elemento per vedere se il centroide è individuato sulla destra o sulla sinsitra della frame, in parole pover essa ci permette di individuare le fasi di attacco del segnale.

In una fase di sustain osserveremo che i campioni sono simili e il centroide è al centro, invece se esso fosse spostato verso destra, i campioni starebbero crescendo, quindi saremmo in fase di attacco; mentro se fosse spostato verso sinistra i campioni starebbero decrescendo e saremmo quindi in fase di release del segnale.

## Esempio di applicazione della ricerca del centroide spettrale

```
import numpy as np
from scipy import signal
import pyACA
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

#leggiamo un file audio(file audio mono)
#[fs, x] = read("daniel.wav")
[fs, x] = read("oboe.wav")
print("SR: ",fs,"\ndimensione del file audio in campioni: ", np.size(x))

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcoliamo la matrice stft
f, t, STFT = signal.stft(x, fs, nperseg=4096, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

#mi calcolo il centroide spettrale
specCent = pyACA.FeatureSpectralCentroid(np.abs(STFT),fs)
print("dimensioni del vettore del centroide spettrale: ",specCent.shape)

#plottiamo il tutto
plt.subplot(2,1,1)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Segnale')
plt.plot(x)

plt.subplot(2,1,2)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Centroide spettrale')
plt.plot(t, specCent)

plt.show()
```
![img](img/vi.png)

Se il suono è fermo e stabile osserviamo come il centroide è stabeile, ciò indica elementi sia del pitch che del timbro a sua volta, indica che globalmente il suono rimane stabile, visualizzando un esesmpio di un oboe.

Per il parlato avremo invece un risultato diverso:
![img](img/vi2.png)

Osserviamo che un file di parlato, che ha anche situazioni di silenzio e rumore di fondo del segnale, non abbiamo lo zero teoricamente ovvero il valore `nan`, in questo caso abbiamo invece che la centroide spettrale schizza verso l'alto nei momenti di silenzio, proprio ad indicare che qualcosa cambia repentinamente nel segnale.

Osserviamo inoltre che il il fonema finale è una _s_ e corrisponde al valore finale del centroide spettrale che va verso l'alto, inoltre in tutta l'ultima frase vi è un'alternanza fra consonanti e vocali, e l'oscillazione della brightness da un'indicazione dell'oscillazione fra consonanti e vocali:
- le vocali sono componenti armoniche bilanciate verso il basso
- le consonanti quando arrivano fanno registrare un picco della brightness, poichè esse sono componenti rumorose che sono filtrate attraverso i risuonatori vocali(naso, bocca e cavità orale)

Quindi già facendo una discriminazione di soglia sul valore della brightness, essa la potremmo usare per capire quali frammenti del parlato sono rumore e quali sono vocali.

In questo script, differentemente da prima, calcoliamo la matrice STFT e poi la mandiamo direttamente in pasto al metodo `FeatureSpectralCentroid()` della libreria `pyACA`.

Ciò ci permette di calcolare la feature piú semplicemente:

```
import numpy as np
import matplotlib.pyplot as plt
import pyACA

#leggiamo un file audio(file audio mono)
path = "daniel.wav"
[fs, x] = pyACA.ToolReadAudio(path)

#normalizziamo il file audio(a 0 dB)
x = x/np.max(abs(x)) #dividiamo il file per l'assoluto del suo massimo

#calcolo del descrittore
[dcs, t] = pyACA.computeFeature("SpectralCentroid",x,fs,iBlockLength=1024, iHopLength=512) #dcs= descrittore centroide spettrale

#plottiamo
plt.plot(t,dcs)
plt.grid()
plt.xlabel('Tempo')
plt.ylabel('Segnale')
plt.show()
```

#### Domanda posta dal docente

Perchè nella fase di fine il centroide cresce per l'oboe?

![img](img/o.png)

## Altri due descrittori

Per casa provare a riprodurre lo stesso tipo di istruzione qua descritto usando altri due descrittori:

- spectral flattness
- spectral flux

Sempre dalla libreria `pyACA`, nel caso di strumenti che possiamo confrontare a nostro piacimento vi sono:
- violino
- pianoforte

Prediligere quindi strumenti con caratteristiche di inviluppo diverse, questi due nuovi descrittori daranno indicazioni sul tempo.

### Descrittore flattness spettrale

Esso è il rapporto tra media geometrica e media aritmetica, è quindi un descrittore scalare il quale comporta, di fare la media geometrica, ovvero dei prodotti fratto la media aritmetica, ovvero le somme.
Invece della somma o della divisione si usano quindi prodotto e radice.

Intutivamente ciò significa che se c'è qualche bin che vale zero, tutto il prodotto della media geometrica varrà zero, se ho quindi uno spettro rigato con sinusoidi o somma di sinusoidi, avrò una piattezza spettrale pari a zero.

Questo descrittore **restituisce un valore che descrive se lo spettro è piú simile a una sintesi additiva o a un rumore**.

Nel rumore bianco tutti i bin hanno un valore diverso da zero.
Nel caso del rumore bianco la media geometrica e aritmetica coincidono, e vale 1.

>Avremmo quindi un valore che identifica se un suono è fatto di parziali o è un rumore.

Questo descrittore **serve per la discriminazione tra parte vocalica e parte consonantica**.
In generale esso lo **posso usare per distinguere le parti di attacco di un suono**, poichè le parti di attacco sono caratterizzate da uno spettro denso o complesso.

### Descrittore SpectralFlux

Il flusso spettrale è un indicatore della differenza tra lo spettro di una frame e lo spettro di una frame successiva, da un'indicazione di quanto varia lo spettro tra una frame ed un'altra.

Ad esempio in un flusso spettrale di una nota lunga di oboe abbiamo variabilità del flusso pari a zero.
Mentre applicandolo al parlato, avremmo una differenza di flusso ampia.
Quale range è posto per questo descrittore?

___

## Da svolgere prima della prossima lezione

- implementare il descrittore della flatness spettrale
- implementare il descrittore della flatness SpectralFlux
- Provare a tirar fuori finestre grafiche per visualizzare contemporaneamente più descrittori(Il consiglio è usare i subplot)
