# Appunti della lezione di lunedì 28 giugno 2021

## Panoramica del corso

Il corso sarà improntato sull'avvicinamento alle tematiche dell'acustica degli strumenti musicali. Tratteremo nel corso vari aspetti legati alla _Music Information Retrival_, d'ora in poi chiamata MIR, che descriviamo brevemente di seguito.

### Cosa significa _Music Information Retrieval_ e perchè è utile?

Per mezzo di varie tecniche informatiche, vengono estratte delle informazioni a diversi livelli di complessità dal materiale audio grezzo.

Esempi di estrazione di informazioni tramite MIR:

- un'informazione rilevante a livello basico potrebbero essere i dB RMS di una traccia audio
- a livello piú astratto, potremmo cercare di decifrare il genere musicale di una traccia analizzata(capiamo subito che per l'identificazione del genere musicale di un materiale sonoro, il materiale deve appartenere a tipologie ben distinte di costruzione musicale; la musica elettronica e contemporanea, fuori da schemi prefissati potrebbe dunque mettere in crisi tale sistema di riconoscimento del genere)

>Capiamo dunque che è importante capire quali siano i metodi di analisi validi per poter procedere ad una corretta analisi del suono.

I classificatori di elementi complessi, cercano quindi di emulare il comportamento umano per il riconoscimento di elementi sonori per mezzo di macchine addestrate con algoritmi che implementano intelligenze artificiali.

Questi classificatori eseguono principalmente 3 operazioni:
1. classificare
2. riconoscere
3. prevedere

### Intelligenze artificiali e umani

Dal punto di vista dell'informazione, gli algoritmi ci inducono a una sorta di visione apocalittica, mentre è da tenere sempre bene a mente che dietro ad ogni macchina vi è una chiara visione umana, che costruisce la macchina in modo che essa funzioni secondo i principi imposti.

### Intelligenze artificiali come scatole nere

Uno dei problemi più stringenti è che gli algoritmi di intelligenza artificiale sono una sorta di scatola nera, perché si riesce in genere ad osservare dei comportamenti emergenti (pilotandoli anche) ma non si sarà complessivamente a conoscenza di ció che verrà fuori dalla macchina.

Si usano quindi delle strategie applicate anche ai sistemi non lineari per cercare di decifrare quali possibili comportamenti tali scatole nere potrebbero avere.

>Di fatto un problema che oggi sta sempre piú emergendo è, ad esempio, il contenuto discriminatorio dei risultati ottenuti tramite algoritmi di AI, provocata spesso dai dati forniti alle macchine nel periodo di apprendimento.

È quindi importante comprendere che: **sia nella raccolta dei dati, sia nell'elaborazione dei dati, non vi è alcuna oggettività ma abbiamo il frutto di una catena di scelte umane e non**.

### Presupposti per il funzionamente delle intelligenze artificiali

Uno dei presupposti di queste macchine è la raccolta di informazioni, abbiamo poi livelli di aggregazione di informazioni, fino poi arrivare a spedire il tutto ai circuiti corticali che ci fanno associare al genere ciò che sentiamo.
Dovremmo quindi capire come funziona l'estrazione di informazioni nel caso di materiale audio.

### Estrazione dei descrittori

Esso è il campo che grazie a strumenti come la trasformata di Fourier
riesce ad estrapolare delle informazioni, anche se esso è sempre stato presentato come un parallelo immanente e di rappresentazione del suono, anche se non lo è per tutto.

Esistono infatti altre tipologie di rappresentazioni del segnale in grado di fornire altre tipologie di informazioni sul suono.

#### Esempio di trasmissione del segnale per mezzo della compressione

Ad esempio se dovessimo trasmettere un segnale sinusoidale da un punto all'altro della terra come potremmo fare?

Dovremmo trasmettere ad esempio 88000 Bytes al secondo con 16 bit...

Se sorgente e destinatario condividono un'informazione, possiamo tranquillamente trasmettere frequenza ed ampiezza dell'oscillatore e potremo trasmettere informazioni a 16 bit, ovvero 4 Byte alla volta; Avremo bisogno di molta meno banda rispetto al caso della trasmissione del segnale brutale.

Tra questi due estremi vi è tutta una varietà possibile di trasmissione di segnale audio.

>L'obiettivo fondamentale sarà trasportare segnale risparmiando banda.

**La condivisione di informazioni tra ricevitore e trasmettitore e la maggiore ottimizzazione renderà in alcuni casi meno dispendioso il passaggio di informazioni**

#### Esempio di trasmissione del segnale, la sintesi additiva

Immaginando di dover trasmettere di ogni segnale sono frequenza e ampiezza, starei quindi risparmiando banda che potrebbe essere usata per altri scopi.

Ad esempio la trasmissione telefonica è una sorta di chimera della voce della persona che parla, non vi più è nulla della persona originale, ma vi è una ricostruzione del suono parlato.

La voce viene campionata, analizzata e rappresentata secondo un certo modello e il ricevitore, condividendo con la sorgente lo stesso modello del segnale, usa i descrittori per ricostruire la voce della persona.

>Quando una serie di informazioni mancano, vengono sintetizzate delle voci robotiche e sintetiche e di fatto non si hanno tutte le informazioni che servono per realizzare una costruzione puntuale.

## Il successo della trasformata di Fourier

Esso fu dovuto all'espandersi in vari rami per la rappresentazione di segnali indipendenti dal tempo e nel quale rappresentazione non pone un problema; nel segnale musicale vi è però l'informazione del tempo che è molto importante, abbiamo quindi bisogno di uno strumento per la rappresentazione di scale temporali diverse.

### Il modello matematico di Fourier: un modello a risoluzione costante

Partendo da un esperimento cercheremo di intendere cosa significhi che il modello di Fourier è a risoluzione costante.

#### L'esperimento
Registrando in un auditorium la Nona di Beethoven, otterremo un file audio da poter analizzare con una FFT o DFT(che osserveremo in seguito).

Otterremo quindi un vettore di numeri complessi contente le informazioni relative a tutto il segnale registrato.

Ipotizzando che essa duri precisamente un'ora, quindi 3600 secondi con una frequenza di campionamento di 44100 Hz, otterremo:
![img](img/44100_cdot_3600_.png)

Essi sono i campioni per un solo canale della registrazione della Sinfonia di Beethoven.

Calcolandone la DFT complessiva, otterró circa 160 milioni di numeri complessi.

Utilizzando 160 milioni di oscillatori potrei realizzare(dando ad ogni oscillatore la propria ampiezza, fase e frequenza) una sintesi additiva che risintetizzi la Sinfonia.

Avremmo quindi la Nona di Beethoven perfettamente riprodotta.

#### Cosa succederà alla fine dell'ora di riproduzione?

La sinfonia ri-inizierà da capo, perché è il risultato della risintesi di una DFT che per definizione genera un segnale periodico.

In questa rappresentazione le periodicità del segnale sono rappresentate per mezzo di 44100 campioni per ogni secondo di segnale.

### La DFT per segnali
Sappiamo che effettuando una DFT, ovvero una Discrete Fourier Transform (dove _Discrete_ indica il fatto che è definita per un segnale tempo discreto) otteniamo un segnale complesso che, se il segnale di partenza è reale, è caratterizzato da un modulo pari (cioè una funzione simmetrica rispetto all'asse delle ordinate) e da una fase dispari (ovvero antisimmetrica).
![img](img/dft.png)

Con la seguente formula ci ricaviamo la frequenza di campionamento che sappiamo essere 2 volte la frequenza di Nyquist.
![img](img/f_s=2f_N.png)
Da cui possiamo ricavare il numero di campioni, ovvero la frequenza di campionamento per il lasso temporale indicato con ∆t:
![img](img/N=f_s_Delta_t.png)

Potremmo quindi cercare altre tipologie di trasformate che non ci diano la parte negativa dello spettro dei moduli segnale, poco utile per i nostri scopi attuali, come la trasformata coseno che non utilizza la parte negativa, risparmiando calcoli, essa è infatti utilizza per la trasmissione GSM.

### Qual è il periodo fondamentale di questo segnale?

Utilizzando quindi la DFT ci chiederemo quale sia ora il periodo del segnale, ma già sapevamo che esso era 3600 secondi, ovvero l'ora di registrazione che avevamo effettuato.

![img](img/T_f=_frac_1_f_f.png)

Questo sarà quindi il periodo fondamentale, ovvero l'inverso della frequenza fondamentale.

Ricordiamoci che stiamo rappresentando la banda di Nyquist attraverso un certo numero di fasori/oscillatori.

Prendendo la banda di Nyquist e dividendola per il numero di oscillatori(diviso a sua volta per 2, poichè sola la metà di essi mi servono per rappresentare la parte positiva, mentre l'altra sezione è simmetrica) avremo quindi:![img](img/frac_f_N_frac_f_.png)

Stiamo tagliando quindi la banda a disposizione in fettine, chiedendoci quanto sia grande ogni fettina, che scopriamo essere l'inverso di ∆t.

Nel caso della Nona di Beethoven avremo una risoluzione frequenziale di 1/3600, che è decisamente molto elevata e girerà con una frequenza di ![img](img/2_cdot_10^-4.png).

La seconda armonica completerà un giro nella metà del tempo, ed avremo quindi un'informazione molto specifica dal punto di vista della frequenza e dal punto di vista del dominio del tempo, 1 ora sarà quindi la fondamentale del segnale registrato.

La conseguenza sarà che mi dovrò sentire tutta la Nona per capire cosa succede all'interno dell'ora intera della Sinfonia.

Ciò porta con se delle conseguenze derivanti dal principio di indeterminazione, per cui:

![img](img/Delta_t_Delta_f_.png)

Quindi il prodotto fra la risoluzione in frequenza e la risoluzione nel tempo è costante ed è uguale ad 1.

>Ciò significa che se desidero maggiore risoluzione in frequenza avrò quindi minore risoluzione nel tempo, e viceversa, poichè il prodotto fra le due risoluzioni deve essere costante.

### Perchè utilizziamo la STFT e non la DFT?

Il principio della STFT (Short Time Fourier Transform) è quello di eseguire l'analisi su una porzione di segnale _sacrificando_ una parte di risoluzione nel dominio della frequenza per guadagnare risoluzione nel dominio del tempo.

Le porzioni di segnali a cui applico la trasformata le chiamo finestre(window) o frame.

Ad esempio se volessimo applicare la stessa definizione ad una finestra di 1000 campioni, prenderemo quindi il segnale nel tempo e lo divideremo in 1000 campioni.

![img](img/1000.png)

Dopodichè trasformando tutto con la DFT otterremmo N campioni complessi.

Avremo quindi una risoluzione in frequenza in base ai campioni ed alla finestra che abbiamo a disposizione:
![Delta_f_=_frac_f.png](img/Delta_f_=_frac_f.png)

In cui _N_ sono i campioni ed f_s è la frequenza di campionamento.

>Osserviamo quindi che calcolandoci il ∆t(inverso del ∆f) otterremo che una banda di segnale sarà rappresentata da un singolo numero:
![img](img/Delta_t_=_frac_1.png)

Avró quindi ogni 23 millisecondi  un vettore di dati che mi dia informazioni sul segnale.

1000 oscillatori mi permetteranno di ricostruire quindi perfettamente il segnale di partenza senza dover aspettare un'ora.

Avremo quindi una latenza molto minore ma sempre fedele al segnale iniziale.

### Diagramma a blocchi: dal segnale discreto alla DFT

Possiamo quindi descrivere il percorso che realizza il segnale per arrivare al risultato finale della DFT, prima di vederne un'applicazione pratica.

![blo1](img/blo1.png)

Esso è suddiviso in 3 sezioni:
1. _framing_, ovvero la suddivisione in finestre (o frame), ovvero tagliando il segnale a fettine
2. moltiplicazione di ogni finestra per una funzione particolare, ciò è utile per diversi scopi:
  - tagliare di netto il segnale, vuol dire realizzare finestre rettangolari di 1000 campioni ad esempio
  - la moltiplicazione ha quindi degli effetti dal punto di vista spettrale che si ripercuteranno sulla ricostruzione del segnale con problemi di fase e di cucitura dei bordi delle finestre
  - per una migliore rappresentazione nel dominio della frequenza, si usa moltiplicare per particolari finestre per permette una miglior giunzione dei bordi delle finestre
  - ognuna delle funzioni per cui si moltiplicano le finestre ha delle caratteristiche
  - ad esempio la trasformata di un seno infinito con finestra rettangolare porterebbe a delle energie laterali in corrispondenza della fine e dell'inizio delle finestre che possono essere viste come artefatti:![ss](img/ss.png)
  tagliando e quindi moltiplicando per una finestra adatta potremmo invece ottenere il risultato sperato senza artefatti:![ta](img/ta.png)
  - tagliando il segnale in N periodi perfettamente sincronizzati all'altezza di riferimento, si ottengono periodi perfettamente tagliati, avremo allora un DFT Pitch-synchronus, ovvero sincronizzata all'altezza
  - **le funzioni moltiplicatrici smussano quindi le discontinuità che si realizzano tagliando _brutalmente_ il segnale in finestre**
3. ne realizziamo la DFT

### Ricavare delle informazioni dai passaggi appena realizzati

Possiamo quindi ora ricavare delle informazioni dai passaggi realizzati per mezzo dei vari blocchi che andranno ad identificare descrittori diversi.

![ddd](img/ddd.png)

Ad esempio dal _framing_ potremmo ricavare dei descrittori temporali come i dati relativi ai dB RMS o passando al dominio della frequenza potremmo ricavarci dei descrittori frequenziali come la centroide spettrale.

**È importante notare che per quanto riguarda il dominio del tempo, si lavorerà nel regime discreto con valori scalari, mentre per il dominio delle frequenze, si passerà al dominio complesso.**

## Un primo esempio di STFT in Python: lo spettrogramma

Come primo esempio di applicazione pratica possiamo realizzare lo spettrogramma di un segnale d'esempio.

Realizzare uno spettrogramma significherà mettere in fila tutte le finestre e visualizzare in un qualche modo.

Avremmo quindi un array `x[n]` ovvero che contiene tutti i campioni del segnale, ed un array `w[n]` che sarà relativo ad ogni finestra che decidiamo avere come grandezza `n`.

![ma](img/ma.png)

Ci costruiremo quindi 2 matrici:

1. matrice delle frame temporali: ovvero la matrice delle finestre nel tempo, una colonna della matrice conterrà una finestra della mia analisi, la seconda colonna i secondi mille campioni etc...

2. matrice nel dominio della frequenza, ovvero le DFT di ogni frame

Avremo quindi in ascissa il tempo corrispondente ad ognuna delle fettine del segnale contenuta in ogni frame

>Lo spettrogramma è il primo tipo di rappresentazione tempo-frequenza che posso ottenere nel momento in cui rappresento ogni finestra di analisi e assegno il colore in base al modulo di ognuno degli elementi della matrice.

Ognuno degli elementi della seconda matrice è chiamato in genere _bin_, ovvero un secchio da cui attingere, all'interno di un bin vi è quindi un intervallo di frequenza e non una sola frequenza.

### Realizzazione dello spettrogramma in Python

Utilizzando le librerie installate e testate come funzionanti sulla nostra macchina:
- `numpy`
- `scipy`
- `matplotlib`

Realizziamo uno spettrogramma sfruttando il metodo di STFT(Short Time Fourier Transform) di `scipy`.

### `import` delle librerie

Dopo aver installato e provato le librerie `numpy`, `scipy` e `matplotlib`, importiamo le 3 librerie(o parte di esse) per poterle usare nel nostro script.

```
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.io.wavfile import read
```

### Lettura di un file

Per mezzo della libreria `scipy` possiamo leggere un file audio mono, grazie al metodo `wavfile` della sezione `io` che abbiamo appena rinominato `read`.

Il file audio utilizzato è disponibile [qui](/audio/daniel.wav).
```
[fs, x] = read("daniel.wav")
print("SR:", fs, "\ndimensione del file audio: ", np.size(x))
```

Leggiamo il file e ne stampiamo alcune informazioni, per controllare se il file è stato letto correttamente.

La lettura del file ci salverà la frequenza di campionamento del file ed ogni campione in x(array di campioni).
### Normalizzazione del file audio

Normalizziamo il file audio a 0 dB per mezzo di una semplice divisione per il valore assoluto del massimo valore del file audio.
```
x = x / np.max(abs(x))
```
### Calcolo della matrice della STFT

Ora utilizzando il file normalizzato utilizziamo parte della libreria `scipy` (`signal`) per poter realizzare la Short Fourier Time Transform.

In essa troviamo un metodo con molti argomenti denominato `stft()`, esso nel nostro caso prenderà:
- il file
- la frequenza di campionamento
- la larghezza della finestra
- l'overlap fra le varie finestre
- la tipologia di finestra

```
f, t, STFT = signal.stft(x, fs, nperseg=1024, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)
```

Al termine dello svolgimento del metodo ci verrà stampata la dimensione della matrice della STFT.

### _Plotting_ della forma d'onda e dello spettrogramma

Inizialmente, anche senza aver realizzato la STFT, possiamo stampare la forma d'onda del file normalizzato:
```
plt.figure(1)
plt.ylabel('segnale')
plt.grid()
plt.plot(x)

plt.figure(2)
plt.pcolormesh(t, f, np.abs(STFT), vmin=0, vmax=np.max(abs(STFT)), shading='auto')
plt.xlabel('Tempo [s]')
plt.ylabel('Frequenza [Hz]')
plt.show()
```
La seconda figura che creeremo sarà invece la figura rappresentante la trasformata di Fourier veloce, appena realizzata, otterremo quindi uno spettrogramma di cui possiamo decidere l'accuratezza per mezzo dei parametri passati nella sezione precedente al metodo `stft()`.

Inseriamo inoltre elementi grafici che ci precisano l'asse delle frequenze e quello del tempo.

### Il risultato finale
```
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

# leggo un file audio (mono)
[fs, x] = read("daniel.wav")
print("SR:", fs, "\ndimensione del file audio: ", np.size(x))

# normalizzo il file
x = x / np.max(abs(x))

# calcolo la matrice stft
f, t, STFT = signal.stft(x, fs, nperseg=1024, noverlap=512, window='blackmanharris')
print("dimensioni della matrice STFT: ", STFT.shape)

# plot
plt.figure(1)
plt.ylabel('segnale')
plt.grid()
plt.plot(x)

plt.figure(2)
plt.pcolormesh(t, f, np.abs(STFT), vmin=0, vmax=np.max(abs(STFT)), shading='auto')
plt.xlabel('Tempo [s]')
plt.ylabel('Frequenza [Hz]')
plt.show()
```

Il seguente script genererà 2 differenti immagini navigabili con:
- la forma d'onda
![plt](img/plt1.png)
- lo spettrogramma
![plt](img/plt.png)
